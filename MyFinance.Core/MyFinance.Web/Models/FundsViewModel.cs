﻿using MyFinance.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFinance.Web.Models
{
    public class FundsViewModel
    {
        public Fund Fund { get; set; }
        public List<Fund> FundList { get; set; }
    }
}