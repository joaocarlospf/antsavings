﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyFinance.Core.Model;

namespace MyFinance.Web.Models
{
    public class RulesViewModel
    {
        public DistributionRule Rule { get; set; }
        public List<DistributionRule> RuleList { get; set; }
    }
}