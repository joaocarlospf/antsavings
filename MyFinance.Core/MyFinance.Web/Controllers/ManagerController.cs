﻿using MyFinance.Core;
using MyFinance.Core.Model;
using MyFinance.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFinance.Web.Controllers
{
    public class ManagerController : Controller
    {
        //
        // GET: /Manager/
        public PartialViewResult SavingsAccountPartial()
        {
            var tvm = new TransactionViewModel();
            using (FinancialQueries queries = new FinancialQueries())
            {
                List<Fund> fundList = queries.GetFunds(User.Identity.Name);
                List<Reserve> reserveList = queries.GetReserves(User.Identity.Name);
                List<SelectListItem> distributionRuleItems = GetListItem(queries.GetDistributionRules(User.Identity.Name));

                tvm.TransactionList = queries.GetTransactions(null, null, DateTime.Today, User.Identity.Name);
                tvm.FundList = GetListItem(fundList, "TODOS");
                tvm.ReserveList = GetListItem(reserveList, "TODOS");
                tvm.Balance = tvm.TransactionList.Sum(t => t.Value);

                List<UpdateFundToDepositViewModel> fundsBalanceList = new List<UpdateFundToDepositViewModel>();
                fundsBalanceList.AddRange(
                    fundList.Select(
                        f => new UpdateFundToDepositViewModel()
                        { FundId = f.ID, FundName = f.Name }
                    )
                );

                DepositViewModel dvm = new DepositViewModel()
                {
                    FundList = GetListItem(fundList, "NENHUM"),
                    ReserveList = GetListItem(reserveList, "NENHUM"),
                    DistributionRuleList = distributionRuleItems,
                    Date = DateTime.Today,
                    fundsBalanceList = fundsBalanceList
                };
                tvm.DepositViewModel = dvm;

                WithdrawViewModel wvm = new WithdrawViewModel()
                {
                    FundList = GetListItem(fundList),
                    ReserveList = GetListItem(reserveList),
                    Date = DateTime.Today
                };
                tvm.WithdrawViewModel = wvm;

                UpdateBalanceViewModel ubvm = new UpdateBalanceViewModel()
                {
                    FundList = GetListItem(fundList),
                    Date = DateTime.Today
                };
                tvm.UpdateBalanceViewModel = ubvm;
            }

            return PartialView("SavingsAccountPartial", tvm);
        }

        [HttpPost]
        public ActionResult GetTransationList(int? fundId, int? reserveId = null)
        {
            if (fundId == -1) fundId = null;
            if (reserveId == -1) reserveId = null;
            using (FinancialQueries queries = new FinancialQueries())
            {
                List<Transaction> transactionList = queries.GetTransactions(
                    fundId, reserveId, DateTime.Today, User.Identity.Name);
                return Json(new
                {
                    balance = transactionList.Sum(t => t.Value).ToString("0.00"),
                    transactionList = RenderPartialViewToString("TransactionListPartial", transactionList)
                });
            }
        }

        public ActionResult GetFundsFromDepositRule(int depositRuleId)
        {
            List<UpdateFundToDepositViewModel> ubvmList = new List<UpdateFundToDepositViewModel>();
            using (FinancialQueries fq = new FinancialQueries())
            {
                if (depositRuleId != -1)
                {
                    int[] funds = fq.GetDistributionRule(depositRuleId).DistributionPercentages.Where(dp => dp.FundID != null).Select(dp => dp.FundID.Value).Distinct().ToArray();
                    return Json(funds);
                }
            }

            return Json(new { });
        }

        [HttpPost]
        public ActionResult UpdateBalance(UpdateBalanceViewModel ubvm)
        {
            using (FinancialOperations fo = new FinancialOperations())
            {
                fo.UpdateProfits(ubvm.Value, ubvm.SelectedFundId, ubvm.Date, User.Identity.Name);
            }

            return SavingsAccountPartial();
        }

        [HttpPost]
        public ActionResult Deposit(DepositViewModel dvm)
        {
            using (FinancialOperations fo = new FinancialOperations())
            {
                Dictionary<int, decimal> balancesBeforeDeposit = new Dictionary<int,decimal>();
                foreach (var fb in dvm.fundsBalanceList)
                    if (fb.Value.HasValue)
                        balancesBeforeDeposit.Add(fb.FundId, fb.Value.Value);

                fo.Deposit(dvm.Value, balancesBeforeDeposit, dvm.Date, dvm.Origin, dvm.SelectedFundId,
                    dvm.SelectedReserveId, dvm.SelectedDistriutionRuleId, User.Identity.Name);
            }

            return SavingsAccountPartial();
        }

        //[HttpPost]
        public ActionResult Withdraw(WithdrawViewModel wvm)
        {
            using (FinancialOperations fo = new FinancialOperations())
            {
                fo.WithDraw(wvm.Value, wvm.Date, wvm.Origin, wvm.SelectedFundId, wvm.SelectedReserveId, User.Identity.Name);
            }

            return SavingsAccountPartial();
        }

        private List<SelectListItem> GetListItem(List<Fund> list, string noneName = null)
        {
            List<SelectListItem> ret = new List<SelectListItem>();
            if (noneName != null)
                ret.Add(new SelectListItem() { Text = noneName, Value = "-1" });
            ret.AddRange(list.Select(f => new SelectListItem() { Text = f.Name, Value = f.ID.ToString() }));
            return ret;
        }

        private List<SelectListItem> GetListItem(List<DistributionRule> list)
        {
            List<SelectListItem> ret = new List<SelectListItem>();
            ret.Add(new SelectListItem() { Text = "NENHUM", Value = "-1" });
            ret.AddRange(list.Select(f => new SelectListItem() { Text = f.Name, Value = f.Id.ToString() }));
            return ret;
        }

        private List<SelectListItem> GetListItem(List<Reserve> list, string noneName = null)
        {
            List<SelectListItem> ret = new List<SelectListItem>();
            if (noneName != null)
                ret.Add(new SelectListItem() { Text = noneName, Value = "-1" });
            ret.AddRange(list.Select(f => new SelectListItem() { Text = f.Name, Value = f.ID.ToString() }));
            return ret;
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

    }
}