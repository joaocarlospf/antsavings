﻿using MyFinance.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyFinance.Core.Model;
using MyFinance.Core;

namespace MyFinance.Web.Controllers
{
    public class FundsController : Controller
    {
        //
        // GET: /Funds/
        public PartialViewResult FundsPartial()
        {
            FundsViewModel fvm = new FundsViewModel();
            using (FinancialQueries fq = new FinancialQueries())
            {
                fvm.FundList = fq.GetFunds(User.Identity.Name);
            }

            fvm.Fund = new Fund();
            
            return PartialView("FundsPartial", fvm);
        }

        public ActionResult AddFund(Fund fund)
        {
            using (FinancialOperations fo = new FinancialOperations())
            {
                fund.UserId = User.Identity.Name;
                fo.AddFund(fund);
            }

            return FundsPartial();
        }

        public ActionResult RemoveFund(int fundId)
        {
            using (FinancialOperations fo = new FinancialOperations())
            {
                fo.RemoveFund(fundId);
            }

            return FundsPartial();
        }
	}
}