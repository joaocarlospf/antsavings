﻿using MyFinance.Core;
using MyFinance.Core.Model;
using MyFinance.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFinance.Web.Controllers
{
    public class RulesController : Controller
    {
        public PartialViewResult RulesPartial()
        {
            List<DistributionRule> ruleList = null;
            using (var queries = new FinancialQueries())
            {
                ruleList = queries.GetDistributionRules(User.Identity.Name);
            }

            RulesViewModel rvm = new RulesViewModel()
            {
                RuleList = ruleList,
                Rule = new DistributionRule()
            };

            return PartialView("RulesPartial", rvm);
        }

        public ActionResult AddRule(RulesViewModel rvm)
        {
            var rule = rvm.Rule;

            using (FinancialOperations fo = new FinancialOperations())
            {
                rule.UserId = User.Identity.Name;
                fo.AddRule(rule);
            }

            return RulesPartial();
        }

        public ActionResult RemoveRule(int ruleId)
        {
            using (FinancialOperations fo = new FinancialOperations())
            {
                fo.RemoveRule(ruleId);
            }

            return RulesPartial();
        }

        public PartialViewResult RulesPercentagePartial(int ruleId)
        {
            List<DistributionPercentage> percentageList = null;
            List<Fund> fundList = null;
            List<Reserve> reserveList = null;
            using (var queries = new FinancialQueries())
            {
                fundList = queries.GetFunds(User.Identity.Name);
                reserveList = queries.GetReserves(User.Identity.Name);
                percentageList = queries.GetPercentageList(ruleId);
            }

            PercentageRuleViewModel prvm = new PercentageRuleViewModel()
            {
                DistributionPercentageList = percentageList,
                DistributionPercentage = new DistributionPercentage(),
                FundsList = GetListItem(fundList, "NONE"),
                ReservesList = GetListItem(reserveList)
            };

            prvm.DistributionPercentage.DistributionRuleId = ruleId;

            return PartialView("RulesPercentagePartial", prvm);
        }

        public ActionResult AddPercentageRule(PercentageRuleViewModel prvm)
        {
            var percentageRule = prvm.DistributionPercentage;

            if (percentageRule.FundID == -1)
                percentageRule.FundID = null;

            using (FinancialOperations fo = new FinancialOperations())
            {
                fo.AddPercentageRule(percentageRule);
            }

            return RulesPercentagePartial(prvm.DistributionPercentage.DistributionRuleId);
        }

        public ActionResult RemovePercentageRule(int percentageRuleId)
        {
            int ruleId;
            using (FinancialOperations fo = new FinancialOperations())
            {
                ruleId = fo.RemovePercentageRule(percentageRuleId);
            }

            return RulesPercentagePartial(ruleId);
        }

        private List<SelectListItem> GetListItem(List<Fund> list, string noneName = null)
        {
            List<SelectListItem> ret = new List<SelectListItem>();
            if (noneName != null)
                ret.Add(new SelectListItem() { Text = noneName, Value = "-1" });
            ret.AddRange(list.Select(f => new SelectListItem() { Text = f.Name, Value = f.ID.ToString() }));
            return ret;
        }

        private List<SelectListItem> GetListItem(List<Reserve> list, string noneName = null)
        {
            List<SelectListItem> ret = new List<SelectListItem>();
            if (noneName != null)
                ret.Add(new SelectListItem() { Text = noneName, Value = "-1" });
            ret.AddRange(list.Select(f => new SelectListItem() { Text = f.Name, Value = f.ID.ToString() }));
            return ret;
        }
	}
}