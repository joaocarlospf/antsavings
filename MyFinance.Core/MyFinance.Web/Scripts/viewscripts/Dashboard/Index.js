﻿function postAndUpdate(url, form, target, modalName) {
    //if ($(form).valid())
    //{
    //$(modalName).modal('hide');


    $(modalName).modal('hide');
    var formdata = $(form).serialize();
    $.post(url, formdata,
        function (data) {
            $(target).html('').append(data);
        });
    //}
}

function postAndUpdateManager(url, form, target, modalName) {
    $(modalName).modal('hide');
    var formdata = $(form).serialize();
    $.post(url, formdata,
        function (data) {
            $(target).html('').append(data);
            $('#fundList').change(reloadGrid);
            $('#reserveList').change(reloadGrid);
        });
}

function adjustScrollArea() {
    $('#rollArea').slimScroll({
        height: 'auto'
    });
}

function loadPartial(url, d, placeToLoad) {
    $.post(url, d,
        function (data) {
            $(placeToLoad).html('').append(data);
        });
}

$(document).ready(function () {

    $('#btnManager').on('click', { btnName: '#btnManager' }, loadPanel);
    $('#btnFunds').on('click', { btnName: '#btnFunds' }, loadPanel);
    $('#btnReserves').on('click', { btnName: '#btnReserves' }, loadPanel);
    $('#btnRules').on('click', { btnName: '#btnRules' }, loadPanel);

    $('#fundList').on("change", reloadGrid);
    $('#reserveList').on("change", reloadGrid);
});
