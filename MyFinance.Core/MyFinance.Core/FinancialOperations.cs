﻿using MyFinance.Core.DAL;
using MyFinance.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core
{
    public class FinancialOperations : IDisposable
    {
        private MyFinanceDataContext _context = new MyFinanceDataContext();

        public void UpdateProfits(decimal value, int fundId, DateTime updateDate, string userId)
        {
            Dictionary<int, decimal> profitPerReserve = new Dictionary<int, decimal>();

            using (FinancialQueries queries = new FinancialQueries(_context))
            {
                List<Reserve> existingReserves = queries.GetReserves(userId);
                foreach (var reserve in existingReserves)
                    profitPerReserve.Add(reserve.ID, 0);

                decimal currentBalance = queries.GetBalance(updateDate, userId, fundId: fundId);
                decimal profitAmount = value - currentBalance;

                if (profitAmount == 0)
                    return;

                DateTime? lastProfitUpdate = queries.GetLastProfitUpdate(fundId);

                if (lastProfitUpdate == null)
                    throw new Exception("It's not possible to update the balance without any transaction.");

                if (lastProfitUpdate >= updateDate)
                    throw new Exception("It's not possible to update the balance before the last balance update date.");

                int profitActionInterval = (int)updateDate.Date.Subtract(lastProfitUpdate.Value.Date).TotalDays;

                List<DateTime> thresholds =
                    _context.Transactions.Where(t => t.Date > lastProfitUpdate && t.Date < updateDate
                        && (t.FundID == fundId || t.FundOriginID == fundId)).Select(t => t.Date).Distinct().ToList();

                for (int i = 0; i <= thresholds.Count; i++)
                {
                    DateTime ini = i == 0 ? lastProfitUpdate.Value : thresholds[i - 1];
                    DateTime fim = i == thresholds.Count ? updateDate : thresholds[i];
                    int interv = (int)fim.Subtract(ini).TotalDays;

                    decimal fundBalance = queries.GetBalance(ini, userId, fundId);
                    foreach (var reserve in existingReserves)
                    {
                        decimal reserveBalance = queries.GetBalance(ini, userId, fundId, reserve.ID);
                        decimal reserveProportion = fundBalance > 0 ? (reserveBalance / fundBalance) : ((decimal)1 / existingReserves.Count);

                        profitPerReserve[reserve.ID] += profitAmount * reserveProportion * ((decimal)interv / profitActionInterval);
                    }
                }
            }

            Profit profit = new Profit()
            {
                FundID = fundId,
                Value = value,
                Date = updateDate,
                Transactions = new List<Transaction>()
            };

            foreach (var reserveProfit in profitPerReserve)
            {
                if (reserveProfit.Value == 0)
                    continue;

                Transaction trans = new Transaction()
                {
                    Date = updateDate,
                    ReserveID = reserveProfit.Key,
                    FundID = fundId,
                    Type = TransactionType.ProfitUpdate,
                    Value = reserveProfit.Value,
                    UserId = userId,
                    ProfitId = 0
                };
                profit.Transactions.Add(trans);
            }

            _context.Profits.Add(profit);
            _context.SaveChanges();
        }

        public void WithDraw(decimal value, DateTime date, string origin, int fundID, int reserveID, string userId)
        {
            // verify balance
            using (FinancialQueries queries = new FinancialQueries(_context))
            {
                decimal balance = queries.GetBalance(date, userId, fundID, reserveID);
                if (balance - value < 0)
                    throw new Exception("There is no balance for this transaction.");
            }

            Transaction trans = new Transaction()
            {
                Date = date,
                ReserveID = reserveID,
                FundID = fundID,
                Type = TransactionType.Debt,
                Value = -value,
                Origins = origin,
                UserId = userId
            };

            _context.Transactions.Add(trans);
            _context.SaveChanges();
        }

        public void Deposit(decimal value, Dictionary<int, decimal> balancesBeforeDeposit, DateTime date, string origin, int? fundId, int? reserveId,
            int? distributionRuleId, string userId)
        {
            if (balancesBeforeDeposit != null)
                foreach (var balancefund in balancesBeforeDeposit)
                    UpdateProfits(balancefund.Value, balancefund.Key, date, userId);

            IEnumerable<DistributionPercentage> dp = (distributionRuleId.HasValue && distributionRuleId != -1)
                ? _context.DistributionPercentages.Where(d => d.DistributionRuleId == distributionRuleId) : null;

            if (dp == null)
            {
                Transaction trans = new Transaction()
                {
                    Date = date,
                    ReserveID = reserveId.Value,
                    FundID = fundId.Value,
                    Type = (int)TransactionType.Deposit,
                    Value = value,
                    Origins = origin,
                    UserId = userId
                };

                _context.Transactions.Add(trans);
            }
            else
            {
                List<Transaction> transactionList = new List<Transaction>();
                foreach (var percentageRule in dp)
                {
                    Transaction trans = new Transaction()
                    {
                        Date = date,
                        ReserveID = percentageRule.ReserveID,
                        FundID = percentageRule.FundID.HasValue ? percentageRule.FundID.Value : fundId.Value,
                        Type = (int)TransactionType.Deposit,
                        Value = value * percentageRule.Percentage,
                        Origins = origin,
                        UserId = userId

                    };
                    transactionList.Add(trans);
                }
                _context.Transactions.AddRange(transactionList);
            }
            _context.SaveChanges();
        }

        private void Deposit(decimal value, DateTime date, int fundID, int reserveID, string origin)
        {
            Transaction trans = new Transaction()
            {
                Date = date,
                ReserveID = reserveID,
                FundID = fundID,
                Type = (int)TransactionType.Deposit,
                Value = value,
                Origins = origin
            };

            _context.Transactions.Add(trans);
            _context.SaveChanges();
        }


        public void Dispose()
        {

        }

        public void AddFund(Fund fund)
        {
            _context.Funds.Add(fund);
            _context.SaveChanges();
        }

        public void RemoveFund(int fundId)
        {
            _context.Funds.Remove(_context.Funds.SingleOrDefault(f => f.ID == fundId));
            _context.SaveChanges();
        }

        public void RemoveReserve(int reserveId)
        {
            _context.Reserves.Remove(_context.Reserves.SingleOrDefault(r => r.ID == reserveId));
            _context.SaveChanges();
        }

        public void AddReserve(Reserve reserve)
        {
            _context.Reserves.Add(reserve);
            _context.SaveChanges();
        }

        public void AddRule(DistributionRule rule)
        {
            _context.DistributionRules.Add(rule);
            _context.SaveChanges();
        }

        public void RemoveRule(int ruleId)
        {
            _context.DistributionRules.Remove(_context.DistributionRules.SingleOrDefault(r => r.Id == ruleId));
            _context.SaveChanges();
        }

        public void AddPercentageRule(DistributionPercentage percentageRule)
        {
            _context.DistributionPercentages.Add(percentageRule);
            _context.SaveChanges();
        }

        public int RemovePercentageRule(int percentageRuleId)
        {
            DistributionPercentage dp = _context.DistributionPercentages.SingleOrDefault(r => r.ID == percentageRuleId);
            int ruleId = dp.DistributionRuleId;
            _context.DistributionPercentages.Remove(dp);
            _context.SaveChanges();

            return ruleId;
        }
    }

    
}
