﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core.Model
{
    public class Reserve
    {
        public int ID { get; set; }

        public string Name { get; set; }

        [StringLength(5)]
        [Display(Name = "Abbreviation")]
        public string NameAbbreviation { get; set; }

        [Display(Name = "Period to Withdraw")]
        public int PeriodToWithdraw { get; set; }

        public string TimeUnit { get; set; }

        public string UserId { get; set; }

        public virtual ICollection<DistributionPercentage> DistributionPercentages { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
