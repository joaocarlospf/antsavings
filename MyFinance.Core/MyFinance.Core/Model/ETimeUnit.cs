﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core.Model
{
    public enum ETimeUnit
    {
        MONTH,
        YEAR
    }
}
