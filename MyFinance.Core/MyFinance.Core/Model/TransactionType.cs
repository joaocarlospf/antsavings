﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core.Model
{
    public enum TransactionType
    {
        [Description("Deposit")]
        Deposit = 0,
        [Description("Debt")]
        Debt = 1,
        [Description("Profit Update")]
        ProfitUpdate = 2
    }
}
