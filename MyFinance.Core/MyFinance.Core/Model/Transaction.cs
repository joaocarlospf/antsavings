﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core.Model
{
    public class Transaction
    {
        public int ID { get; set; }

        public decimal Value { get; set; }

        public TransactionType Type { get; set; }

        public System.DateTime Date { get; set; }

        public int ReserveID { get; set; }
        public virtual Reserve Reserve { get; set; }

        public int FundID { get; set; }
        public virtual Fund Fund { get; set; }

        public string Origins { get; set; }

        public int? FundOriginID { get; set; }
        public virtual Fund FundOrigin { get; set; }

        public int? ProfitId { get; set; }
        public virtual Profit Profit { get; set; }

        public string UserId { get; set; }




    }
}
