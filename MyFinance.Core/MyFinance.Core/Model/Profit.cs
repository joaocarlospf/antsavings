﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core.Model
{
    public class Profit
    {
        public int ID { get; set; }

        public decimal Value { get; set; }

        public System.DateTime Date { get; set; }

        [ForeignKey("Fund")]
        public int FundID { get; set; }
        public virtual Fund Fund{ get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

    }
}
