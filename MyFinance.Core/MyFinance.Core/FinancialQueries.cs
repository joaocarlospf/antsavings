﻿using MyFinance.Core.DAL;
using MyFinance.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Core
{
    public class FinancialQueries : IDisposable
    {
        private MyFinanceDataContext _context;

        public FinancialQueries()
        {
            _context = new MyFinanceDataContext();
        }

        public FinancialQueries(MyFinanceDataContext context)
        {
            _context = context;
        }

        public DateTime? GetLastProfitUpdate(int fundId)
        {
            IEnumerable<Profit> profits = _context.Profits.Where(p => p.FundID == fundId);

            if (profits.Count() == 0)
            {
                IEnumerable<Transaction> transations = _context.Transactions.Where(t => t.FundID == fundId);
                if (transations.Count() == 0) return null;
                return transations.Min(t => t.Date);
            }
                

            return _context.Profits.Max(p => p.Date);
        }

        /// <summary>
        /// The balance resulted from every transaction in the fund/reserve until the date (including itself) passed as parameter.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="fundId"></param>
        /// <param name="reserveId"></param>
        /// <returns></returns>
        public decimal GetBalance(DateTime date, string userId, int? fundId = null, int? reserveId = null)
        {
            List<Transaction> transactions = GetTransactions(fundId, reserveId, date, userId);

            return transactions.Sum(t => t.Value);
        }

        public List<Transaction> GetTransactions(int? fundID, int? reserveID, DateTime date, string userId)
        {
            return _context.Transactions.Where(t => t.UserId == userId && t.Date <= date && (fundID == null || t.FundID == fundID) && (reserveID == null || t.ReserveID == reserveID)).OrderBy(t => t.Date).ToList();
        }

        public void Dispose()
        {
        }

        public List<Fund> GetFunds(string userId)
        {
            return _context.Funds.Where(f => f.UserId == userId).ToList();
        }

        public List<Reserve> GetReserves(string userId)
        {
            return _context.Reserves.Where(r => r.UserId == userId).ToList();
        }

        public List<DistributionRule> GetDistributionRules(string userId)
        {
            return _context.DistributionRules.Where(d => d.UserId == userId).ToList();
        }

        public List<DistributionPercentage> GetPercentageList(int ruleId)
        {
            return _context.DistributionPercentages.Where(dp => dp.DistributionRuleId == ruleId).ToList();
        }

        public DistributionRule GetDistributionRule(int depositRuleId)
        {
            return _context.DistributionRules.SingleOrDefault(dr => dr.Id == depositRuleId);
        }

        public Fund GetFund(int fundId)
        {
            return _context.Funds.SingleOrDefault(f => f.ID == fundId);
        }
    }
}
